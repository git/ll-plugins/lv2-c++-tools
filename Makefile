PACKAGE_NAME = lv2-c++-tools
PACKAGE_VERSION = $(shell ./VERSION)
PKG_DEPS = \
	gtkmm-2.4>=2.8.8 \
	redland>=1.0.9

ARCHIVES = liblv2-plugin.a liblv2-gui.a
PROGRAMS = lv2peg
DATAPACKS = lv2soname common_headers

# Internal headers that should not be included by user code.
common_headers_FILES = \
	debug.hpp \
	lv2.h \
	lv2_event.h \
	lv2_event_helpers.h \
	lv2_osc.h \
	lv2_uri_map.h \
	lv2_saverestore.h \
	lv2_contexts.h \
	lv2types.hpp \
	lv2_ui_old.h \
	lv2_ui_presets.h
common_headers_SOURCEDIR = lv2cxx_common
common_headers_INSTALLDIR = $(pkgincludedir)/lv2cxx_common

# The static plugin library with headers
liblv2-plugin_a_SOURCES = lv2plugin.cpp
liblv2-plugin_a_HEADERS = \
	lv2plugin.hpp \
	lv2synth.hpp
liblv2-plugin_a_CFLAGS = -Ilibraries/lv2plugin -I. -Ilv2cxx_common
liblv2-plugin_a_SOURCEDIR = lv2plugin
liblv2-plugin_a_INSTALLDIR = $(libdir)

# The static GUI library with headers
liblv2-gui_a_SOURCES = lv2gui.cpp
liblv2-gui_a_HEADERS = \
	lv2gui.hpp
liblv2-gui_a_CFLAGS = `pkg-config --cflags gtkmm-2.4` -I. -Ilv2cxx_common
liblv2-gui_a_SOURCEDIR = lv2gui
liblv2-gui_a_INSTALLDIR = $(libdir)

# lv2peg
lv2peg_SOURCES = lv2peg.cpp
lv2peg_CFLAGS = -DVERSION=\"$(PACKAGE_VERSION)\" `pkg-config --cflags redland`
lv2peg_LDFLAGS = `pkg-config --libs redland`
lv2peg_SOURCEDIR = lv2peg

# lv2soname
lv2soname_FILES = lv2soname
lv2soname_SOURCEDIR = lv2soname
lv2soname_INSTALLDIR = $(bindir)

# extra files
DOCS = COPYING AUTHORS README ChangeLog
PCFILES = lv2plugin/lv2-plugin.pc lv2gui/lv2-gui.pc
EXTRA_DIST = Doxyfile


# Do the magic
include Makefile.template

# Generate Doxygen documentation with the right version number
dox:
	cat Doxyfile | sed s@VERSION_SUBST@$(PACKAGE_VERSION)@ > Doxyfile.subst
	doxygen Doxyfile.subst
	rm Doxyfile.subst
